"""
Python for everyone 1: Week 2 - Variables
Kennedy Gunn
Goal: compute and compare mileage
and fuel use across small and large cars
"""

print("Welcome to road trip efficiency comparisons!")
# variable names always:
# start with a lowercase letter, and not a number
# __main__ are system variables and are special
# assigning a string literal on the right
# to the variable on the left
small_vehicle = "2018 Mitsubishi Outlander Sport"
large_vehicle = "2015 Cadillac Escalade 2WD"
small_vehicle_mpg = 26.0
large_vehicle_mpg = 17.0
print(type(large_vehicle_mpg))
small_vehicle_url = "https://www.fueleconomy.gov/feg/Find.do?action=sbs&id=39677"
large_vehicle_url = "https://www.fueleconomy.gov/feg/Find.do?action=sbs&id=35826"
print("The ", small_vehicle, " can drive ", small_vehicle_mpg, " per gallon of gasoline")
print("Info can be found at ", small_vehicle_url)
print("The ", large_vehicle, " can drive ", large_vehicle_mpg, " per gallon of gasoline")
print("Info can be found at ", large_vehicle_url)

print("----------MILEAGE----------")
print("Road trip! How many miles? (decimal places okay)")
# read in keyboard input, turn it into a float, store in trip_miles
trip_miles = float(input())
# gallons computed by dividing trip length by mileage per gallon
small_vehicle_gallons = trip_miles / small_vehicle_mpg
large_vehicle_gallons = trip_miles / large_vehicle_mpg
print("The" , small_vehicle, "will require", small_vehicle_gallons, "gallons to travel", trip_miles, "miles")
print("The" , large_vehicle, "will require", large_vehicle_gallons, "gallons to travel", trip_miles, "miles")
difference_vehicle_gallons = large_vehicle_gallons - small_vehicle_gallons
print("The", large_vehicle, "will require", difference_vehicle_gallons, "more gallons to travel", trip_miles, "miles than the", small_vehicle)

print("-----------AVERAGE COST OF GAS-----------")
print("What is the average cost of gasoline on your trip?")
average_cost_gasoline = float(input())
small_total_cost = small_vehicle_gallons * average_cost_gasoline
large_total_cost = large_vehicle_gallons * average_cost_gasoline
print("The cost for your trip with the", small_vehicle, "will be", small_total_cost)
print("The cost for your trip with the", large_vehicle, "will be", large_total_cost)
difference_total_cost = large_total_cost - small_total_cost
print("The", large_vehicle, "will cost you", difference_total_cost, "more than the", small_vehicle)

print("--------------TRAVEL TIME-------------")
expected_highway_speed = 75.0
expected_local_speed = 35.0
print("How much faster do you drive on the highway than the speed limit?")
user_faster_highway = float(input())
print("How much faster do you drive on local roads than the speed limit?")
user_faster_local = float(input())
user_speed_highway = expected_highway_speed + user_faster_highway
user_speed_local = expected_local_speed + user_faster_local
expected_time_highway = trip_miles / user_speed_highway
expected_time_local = trip_miles / user_speed_local
print("If you drive on the highway in the", small_vehicle, "or the", large_vehicle, "you will be traveling for", expected_time_highway, "hours")
print("If you drive on the local roads in the", small_vehicle, "or the", large_vehicle, "you will be traveling for", expected_time_local, "hours")
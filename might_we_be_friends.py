print("*******Might We Be Friends?********")
print("What is your name?")
candidate = input()
print("Hello", candidate, "please answer these questions truthfully!")
user_score = 0

print("Question 1: Do you like dogs? (yes = 1 no = 2)")
q1_dogs = int(input())
if q1_dogs == 1:
    print("Cool. Me too.")
    user_score = user_score + 10
else:
    print("What! How do you not like dogs?!?!!?")
    user_score = user_score - 10
print("Your new score is", user_score)

print("Question 2: Do you like tacos? (yes = 1 no = 2)")
q2_tacos = int(input())
if q2_tacos == 1:
    print("Mexican food is my favorite!!!")
    user_score = user_score + 10
else:
    print("OMG how??!!!????")
    user_score = user_score - 10
print("Your new score is", user_score)

print("Question 3: Did you sleep 8 hours last night? (yes = 1 no = 2)")
q3_sleep = int(input())
if q3_sleep == 2:
    print("So relatable")
    user_score = user_score + 10
else:
    print("Couldn't be me :(")
    user_score = user_score - 10
print("Your new score is", user_score)

print("Question 4: Do you live on campus? (yes = 1 no = 2)")
q4_campus = int(input())
if q4_campus == 1:
    print("Living in the dorm is so crazy!")
    user_score = user_score + 10
else:
    print("I'm so jealous of you.")
    user_score = user_score - 10
print("Your new score is", user_score)

print("Question 5: Do you like Christmas or Thanksgiving better? (Christmas = 1 Thanksgiving = 2)")
q5_holiday = int(input())
if q5_holiday == 1:
    print("I love Christmas so much.")
    user_score = user_score + 10
else:
    print("Thanksgiving is only good for the food.")
    user_score = user_score - 10
print("Your new score is", user_score)

print("Question 6: Do you like the dining hall food? (yes = 1 no = 2)")
q6_food = int(input())
if q6_food == 2:
    print("I'm glad were on the same page.")
    user_score = user_score + 10
else:
    print("Wait really?")
    user_score = user_score - 10
print("Your new score is", user_score)

print(candidate, "your final score is", user_score)
max_pts = 60
candidate_percent = (user_score / max_pts) * 100
print("We are {:03.2f} percent compatible!".format(candidate_percent))

if user_score == 60:
    print("We are 100% bffs!!! You received a perfect score.")
elif user_score >= 40:
    print("OK we are pretty compatible. I'll be your friend.")
elif user_score <= 30:
    print("Sorry we are not alike. I cannot be your friend.")
else:
    print("You must have made a mistake please try again.")